import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { flatMap } from 'rxjs/operators';

@Component({
  selector: 'jeff-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'user';
  fields: string[] = [];
  form: FormGroup;

  params: Observable<any> = of([
    { name: 'bob' },
    { name: 'bob2' },
    { name: 'bob3' },
    { name: 'bob4' },
    { name: 'bob5' }
  ]);

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({});
  }

  ngOnInit() {
    const sub = this.params.pipe(flatMap(it => it)).subscribe((it: any) => {
      this.form.addControl(it.name, new FormControl(''));
      this.fields = [...this.fields, it.name];
      console.log('add');
    });
    console.log('unsub');
    sub.unsubscribe();
  }

  test() {
    console.log(this.form.getRawValue());
  }
}
