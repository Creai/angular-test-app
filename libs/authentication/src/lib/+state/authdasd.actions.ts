import {Action} from '@ngrx/store';
import {Entity} from './authdasd.reducer';

export enum AuthdasdActionTypes {
 LoadAuthdasd = "[Authdasd] Load Authdasd",
 AuthdasdLoaded = "[Authdasd] Authdasd Loaded",
 AuthdasdLoadError = "[Authdasd] Authdasd Load Error"
}

export class LoadAuthdasd implements Action {
 readonly type = AuthdasdActionTypes.LoadAuthdasd;
}

export class AuthdasdLoadError implements Action {
 readonly type = AuthdasdActionTypes.LoadAuthdasd;
 constructor(public payload: any) { }
}

export class AuthdasdLoaded implements Action {
 readonly type = AuthdasdActionTypes.AuthdasdLoaded;
 constructor(public payload: Entity[]) { }
}

export type AuthdasdAction = LoadAuthdasd | AuthdasdLoaded | AuthdasdLoadError;

export const fromAuthdasdActions = {
  LoadAuthdasd,
  AuthdasdLoaded,
  AuthdasdLoadError
};
