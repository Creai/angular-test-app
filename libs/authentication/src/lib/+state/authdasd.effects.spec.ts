import {TestBed, async} from '@angular/core/testing';

import {Observable} from 'rxjs';

import { EffectsModule } from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {provideMockActions} from '@ngrx/effects/testing';

import { NxModule } from '@nrwl/nx';
import {DataPersistence} from '@nrwl/nx';
import {hot} from '@nrwl/nx/testing';

import { AuthdasdEffects } from './authdasd.effects';
import { LoadAuthdasd, AuthdasdLoaded } from './authdasd.actions';

describe('AuthdasdEffects', () => {
  let actions: Observable<any>;
  let effects: AuthdasdEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NxModule.forRoot(),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([])
      ],
      providers: [
        AuthdasdEffects,
        DataPersistence,
        provideMockActions(() => actions)
      ],
    });

    effects = TestBed.get(AuthdasdEffects);
  });

  describe('loadAuthdasd$', () => {
    it('should work', () => {
      actions = hot('-a-|', {a: new LoadAuthdasd()});
      expect(effects.loadAuthdasd$).toBeObservable(
        hot('-a-|', {a: new AuthdasdLoaded([])})
      );
    });
  });
});
