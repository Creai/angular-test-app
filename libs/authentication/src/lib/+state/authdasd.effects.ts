import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';

import { AuthdasdState  } from './authdasd.reducer';
import { LoadAuthdasd, AuthdasdLoaded, AuthdasdLoadError, AuthdasdActionTypes } from './authdasd.actions';

@Injectable()
export class AuthdasdEffects {
 @Effect() loadAuthdasd$ = this.dataPersistence.fetch(AuthdasdActionTypes.LoadAuthdasd, {
   run: (action: LoadAuthdasd, state: AuthdasdState) => {
     // Your custom REST 'load' logic goes here. For now just return an empty list...
     return new AuthdasdLoaded([]);
   },

   onError: (action: LoadAuthdasd, error) => {
     console.error('Error', error);
     return new AuthdasdLoadError(error);
   }
 });

 constructor(
   private actions$: Actions,
   private dataPersistence: DataPersistence<AuthdasdState>) { }
}
