import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';

import { AuthdasdState } from './authdasd.reducer';
import { authdasdQuery } from './authdasd.selectors';
import { LoadAuthdasd } from './authdasd.actions';

@Injectable()
export class AuthdasdFacade {

  loaded$ = this.store.select(authdasdQuery.getLoaded);
  allAuthdasd$ = this.store.select(authdasdQuery.getAllAuthdasd);
  selectedAuthdasd$ = this.store.select(authdasdQuery.getSelectedAuthdasd);
  
  constructor( private store: Store<AuthdasdState> ) { }
 
  loadAll() {
    this.store.dispatch(new LoadAuthdasd());
  }  
}
