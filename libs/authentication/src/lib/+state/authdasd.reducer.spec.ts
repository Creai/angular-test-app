import { AuthdasdLoaded } from './authdasd.actions';
import { AuthdasdState, Entity, initialState, authdasdReducer } from './authdasd.reducer';

describe('Authdasd Reducer', () => {
  const getAuthdasdId = (it) => it['id'];
  let createAuthdasd;

  beforeEach(() => {
     createAuthdasd = ( id:string, name = '' ): Entity => ({
       id,
       name: name || `name-${id}`
     });
  });

  describe('valid Authdasd actions ', () => {
    it('should return set the list of known Authdasd', () => {
        const authdasds = [createAuthdasd( 'PRODUCT-AAA' ),createAuthdasd( 'PRODUCT-zzz' )];
        const action = new AuthdasdLoaded(authdasds);
        const result : AuthdasdState = authdasdReducer(initialState, action);
        const selId : string = getAuthdasdId(result.list[1]);

        expect(result.loaded).toBe(true);
        expect(result.list.length).toBe(2);
        expect(selId).toBe('PRODUCT-zzz');
    });
  });

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;
      const result = authdasdReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
