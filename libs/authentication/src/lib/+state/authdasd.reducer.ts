import { AuthdasdAction, AuthdasdActionTypes } from './authdasd.actions';

/**
 * Interface for the 'Authdasd' data used in
 *  - AuthdasdState, and
 *  - authdasdReducer
 *
 *  Note: replace if already defined in another module
 */

/* tslint:disable:no-empty-interface */
export interface Entity {
};

export interface AuthdasdState {
  list        : Entity[];             // list of Authdasd; analogous to a sql normalized table
  selectedId ?: string | number;      // which Authdasd record has been selected
  loaded      : boolean;              // has the Authdasd list been loaded
  error      ?: any;                  // last none error (if any)
};

export const initialState: AuthdasdState = {
  list : [ ],
  loaded : false
};

export function authdasdReducer(
  state: AuthdasdState = initialState, 
  action: AuthdasdAction): AuthdasdState
{
  switch (action.type) {
    case AuthdasdActionTypes.AuthdasdLoaded: {
      state = {
        ...state,
        list  : action.payload,
        loaded: true
      };
      break;
    }
  }
  return state;
}
