import { Entity, AuthdasdState  } from './authdasd.reducer';
import { authdasdQuery } from './authdasd.selectors';

describe('Authdasd Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getAuthdasdId = (it) => it['id'];

  let storeState;

  beforeEach(() => {
     const createAuthdasd = ( id:string, name = '' ): Entity => ({
       id,
       name: name || `name-${id}`
     });
     storeState = {
      authdasd : {
        list : [
            createAuthdasd( 'PRODUCT-AAA' ),
            createAuthdasd( 'PRODUCT-BBB' ),
            createAuthdasd( 'PRODUCT-CCC' )
          ],
          selectedId : 'PRODUCT-BBB',
          error      : ERROR_MSG,
          loaded     : true
       }
     };
  });

  describe('Authdasd Selectors', () => {

    it('getAllAuthdasd() should return the list of Authdasd', () => {
      const results = authdasdQuery.getAllAuthdasd(storeState);
      const selId = getAuthdasdId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelectedAuthdasd() should return the selected Entity', () => {
      const result = authdasdQuery.getSelectedAuthdasd(storeState);
      const selId = getAuthdasdId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getLoaded() should return the current \'loaded\' status', () => {
      const result = authdasdQuery.getLoaded(storeState);

      expect(result).toBe(true);
    });

    it('getError() should return the current \'error\' storeState', () => {
      const result = authdasdQuery.getError(storeState);

      expect(result).toBe(ERROR_MSG);
    });

  });
});
