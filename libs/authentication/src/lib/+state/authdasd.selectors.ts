import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthdasdState } from './authdasd.reducer';

// Lookup the 'Authdasd' feature state managed by NgRx
const getAuthdasdState = createFeatureSelector<AuthdasdState>('authdasd');

const getLoaded = createSelector( getAuthdasdState, (state:AuthdasdState) => state.loaded );
const getError = createSelector( getAuthdasdState, (state:AuthdasdState) => state.error );

const getAllAuthdasd = createSelector( getAuthdasdState, getLoaded, (state:AuthdasdState, isLoaded) => {
  return isLoaded ? state.list : [ ];
});
const getSelectedId = createSelector( getAuthdasdState, (state:AuthdasdState) => state.selectedId );
const getSelectedAuthdasd = createSelector( getAllAuthdasd, getSelectedId, (authdasd, id) => {
    const result = authdasd.find(it => it['id'] === id);
    return result ? Object.assign({}, result) : undefined;
});

export const authdasdQuery = {
  getLoaded,
  getError,
  getAllAuthdasd,
  getSelectedAuthdasd
};
