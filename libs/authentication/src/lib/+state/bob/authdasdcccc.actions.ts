import {Action} from '@ngrx/store';
import {Entity} from './authdasdcccc.reducer';

export enum AuthdasdccccActionTypes {
 LoadAuthdasdcccc = "[Authdasdcccc] Load Authdasdcccc",
 AuthdasdccccLoaded = "[Authdasdcccc] Authdasdcccc Loaded",
 AuthdasdccccLoadError = "[Authdasdcccc] Authdasdcccc Load Error"
}

export class LoadAuthdasdcccc implements Action {
 readonly type = AuthdasdccccActionTypes.LoadAuthdasdcccc;
}

export class AuthdasdccccLoadError implements Action {
 readonly type = AuthdasdccccActionTypes.LoadAuthdasdcccc;
 constructor(public payload: any) { }
}

export class AuthdasdccccLoaded implements Action {
 readonly type = AuthdasdccccActionTypes.AuthdasdccccLoaded;
 constructor(public payload: Entity[]) { }
}

export type AuthdasdccccAction = LoadAuthdasdcccc | AuthdasdccccLoaded | AuthdasdccccLoadError;

export const fromAuthdasdccccActions = {
  LoadAuthdasdcccc,
  AuthdasdccccLoaded,
  AuthdasdccccLoadError
};
