import {TestBed, async} from '@angular/core/testing';

import {Observable} from 'rxjs';

import { EffectsModule } from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {provideMockActions} from '@ngrx/effects/testing';

import { NxModule } from '@nrwl/nx';
import {DataPersistence} from '@nrwl/nx';
import {hot} from '@nrwl/nx/testing';

import { AuthdasdccccEffects } from './authdasdcccc.effects';
import { LoadAuthdasdcccc, AuthdasdccccLoaded } from './authdasdcccc.actions';

describe('AuthdasdccccEffects', () => {
  let actions: Observable<any>;
  let effects: AuthdasdccccEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NxModule.forRoot(),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([])
      ],
      providers: [
        AuthdasdccccEffects,
        DataPersistence,
        provideMockActions(() => actions)
      ],
    });

    effects = TestBed.get(AuthdasdccccEffects);
  });

  describe('loadAuthdasdcccc$', () => {
    it('should work', () => {
      actions = hot('-a-|', {a: new LoadAuthdasdcccc()});
      expect(effects.loadAuthdasdcccc$).toBeObservable(
        hot('-a-|', {a: new AuthdasdccccLoaded([])})
      );
    });
  });
});
