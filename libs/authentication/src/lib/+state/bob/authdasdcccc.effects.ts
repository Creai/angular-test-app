import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';

import { AuthdasdccccState  } from './authdasdcccc.reducer';
import { LoadAuthdasdcccc, AuthdasdccccLoaded, AuthdasdccccLoadError, AuthdasdccccActionTypes } from './authdasdcccc.actions';

@Injectable()
export class AuthdasdccccEffects {
 @Effect() loadAuthdasdcccc$ = this.dataPersistence.fetch(AuthdasdccccActionTypes.LoadAuthdasdcccc, {
   run: (action: LoadAuthdasdcccc, state: AuthdasdccccState) => {
     // Your custom REST 'load' logic goes here. For now just return an empty list...
     return new AuthdasdccccLoaded([]);
   },

   onError: (action: LoadAuthdasdcccc, error) => {
     console.error('Error', error);
     return new AuthdasdccccLoadError(error);
   }
 });

 constructor(
   private actions$: Actions,
   private dataPersistence: DataPersistence<AuthdasdccccState>) { }
}
