import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { readFirst } from '@nrwl/nx/testing';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { NxModule } from '@nrwl/nx';

import { AuthdasdccccEffects } from './authdasdcccc.effects';
import { AuthdasdccccFacade } from './authdasdcccc.facade';

import { authdasdccccQuery } from './authdasdcccc.selectors';
import { LoadAuthdasdcccc, AuthdasdccccLoaded } from './authdasdcccc.actions';
import {
  AuthdasdccccState,
  Entity,
  initialState,
  authdasdccccReducer
} from './authdasdcccc.reducer';

interface TestSchema {
  'authdasdcccc' : AuthdasdccccState
}

describe('AuthdasdccccFacade', () => {
  let facade: AuthdasdccccFacade;
  let store: Store<TestSchema>;
  let createAuthdasdcccc;

  beforeEach(() => {
    createAuthdasdcccc = ( id:string, name = '' ): Entity => ({
       id,
       name: name || `name-${id}`
    });
  });

  describe('used in NgModule', () => {

    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature('authdasdcccc', authdasdccccReducer, { initialState }),
          EffectsModule.forFeature([AuthdasdccccEffects])
        ],
        providers: [AuthdasdccccFacade]
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule,
        ]
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.get(Store);
      facade = TestBed.get(AuthdasdccccFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async (done) => {
      try {
        let list = await readFirst(facade.allAuthdasdcccc$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.loadAll();

        list = await readFirst(facade.allAuthdasdcccc$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });

    /**
     * Use `AuthdasdccccLoaded` to manually submit list for state management
     */
    it('allAuthdasdcccc$ should return the loaded list; and loaded flag == true', async (done) => {
      try {
        let list = await readFirst(facade.allAuthdasdcccc$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        store.dispatch(new AuthdasdccccLoaded([
          createAuthdasdcccc('AAA'),
          createAuthdasdcccc('BBB')
        ]));

        list = await readFirst(facade.allAuthdasdcccc$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(2);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });
  });

});
