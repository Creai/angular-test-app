import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';

import { AuthdasdccccState } from './authdasdcccc.reducer';
import { authdasdccccQuery } from './authdasdcccc.selectors';
import { LoadAuthdasdcccc } from './authdasdcccc.actions';

@Injectable()
export class AuthdasdccccFacade {

  loaded$ = this.store.select(authdasdccccQuery.getLoaded);
  allAuthdasdcccc$ = this.store.select(authdasdccccQuery.getAllAuthdasdcccc);
  selectedAuthdasdcccc$ = this.store.select(authdasdccccQuery.getSelectedAuthdasdcccc);
  
  constructor( private store: Store<AuthdasdccccState> ) { }
 
  loadAll() {
    this.store.dispatch(new LoadAuthdasdcccc());
  }  
}
