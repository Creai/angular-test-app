import { AuthdasdccccLoaded } from './authdasdcccc.actions';
import { AuthdasdccccState, Entity, initialState, authdasdccccReducer } from './authdasdcccc.reducer';

describe('Authdasdcccc Reducer', () => {
  const getAuthdasdccccId = (it) => it['id'];
  let createAuthdasdcccc;

  beforeEach(() => {
     createAuthdasdcccc = ( id:string, name = '' ): Entity => ({
       id,
       name: name || `name-${id}`
     });
  });

  describe('valid Authdasdcccc actions ', () => {
    it('should return set the list of known Authdasdcccc', () => {
        const authdasdccccs = [createAuthdasdcccc( 'PRODUCT-AAA' ),createAuthdasdcccc( 'PRODUCT-zzz' )];
        const action = new AuthdasdccccLoaded(authdasdccccs);
        const result : AuthdasdccccState = authdasdccccReducer(initialState, action);
        const selId : string = getAuthdasdccccId(result.list[1]);

        expect(result.loaded).toBe(true);
        expect(result.list.length).toBe(2);
        expect(selId).toBe('PRODUCT-zzz');
    });
  });

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;
      const result = authdasdccccReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
