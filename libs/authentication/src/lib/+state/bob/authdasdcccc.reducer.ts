import { AuthdasdccccAction, AuthdasdccccActionTypes } from './authdasdcccc.actions';

/**
 * Interface for the 'Authdasdcccc' data used in
 *  - AuthdasdccccState, and
 *  - authdasdccccReducer
 *
 *  Note: replace if already defined in another module
 */

/* tslint:disable:no-empty-interface */
export interface Entity {
};

export interface AuthdasdccccState {
  list        : Entity[];             // list of Authdasdcccc; analogous to a sql normalized table
  selectedId ?: string | number;      // which Authdasdcccc record has been selected
  loaded      : boolean;              // has the Authdasdcccc list been loaded
  error      ?: any;                  // last none error (if any)
};

export const initialState: AuthdasdccccState = {
  list : [ ],
  loaded : false
};

export function authdasdccccReducer(
  state: AuthdasdccccState = initialState, 
  action: AuthdasdccccAction): AuthdasdccccState
{
  switch (action.type) {
    case AuthdasdccccActionTypes.AuthdasdccccLoaded: {
      state = {
        ...state,
        list  : action.payload,
        loaded: true
      };
      break;
    }
  }
  return state;
}
