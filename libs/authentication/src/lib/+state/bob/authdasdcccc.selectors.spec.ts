import { Entity, AuthdasdccccState  } from './authdasdcccc.reducer';
import { authdasdccccQuery } from './authdasdcccc.selectors';

describe('Authdasdcccc Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getAuthdasdccccId = (it) => it['id'];

  let storeState;

  beforeEach(() => {
     const createAuthdasdcccc = ( id:string, name = '' ): Entity => ({
       id,
       name: name || `name-${id}`
     });
     storeState = {
      authdasdcccc : {
        list : [
            createAuthdasdcccc( 'PRODUCT-AAA' ),
            createAuthdasdcccc( 'PRODUCT-BBB' ),
            createAuthdasdcccc( 'PRODUCT-CCC' )
          ],
          selectedId : 'PRODUCT-BBB',
          error      : ERROR_MSG,
          loaded     : true
       }
     };
  });

  describe('Authdasdcccc Selectors', () => {

    it('getAllAuthdasdcccc() should return the list of Authdasdcccc', () => {
      const results = authdasdccccQuery.getAllAuthdasdcccc(storeState);
      const selId = getAuthdasdccccId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelectedAuthdasdcccc() should return the selected Entity', () => {
      const result = authdasdccccQuery.getSelectedAuthdasdcccc(storeState);
      const selId = getAuthdasdccccId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getLoaded() should return the current \'loaded\' status', () => {
      const result = authdasdccccQuery.getLoaded(storeState);

      expect(result).toBe(true);
    });

    it('getError() should return the current \'error\' storeState', () => {
      const result = authdasdccccQuery.getError(storeState);

      expect(result).toBe(ERROR_MSG);
    });

  });
});
