import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthdasdccccState } from './authdasdcccc.reducer';

// Lookup the 'Authdasdcccc' feature state managed by NgRx
const getAuthdasdccccState = createFeatureSelector<AuthdasdccccState>('authdasdcccc');

const getLoaded = createSelector( getAuthdasdccccState, (state:AuthdasdccccState) => state.loaded );
const getError = createSelector( getAuthdasdccccState, (state:AuthdasdccccState) => state.error );

const getAllAuthdasdcccc = createSelector( getAuthdasdccccState, getLoaded, (state:AuthdasdccccState, isLoaded) => {
  return isLoaded ? state.list : [ ];
});
const getSelectedId = createSelector( getAuthdasdccccState, (state:AuthdasdccccState) => state.selectedId );
const getSelectedAuthdasdcccc = createSelector( getAllAuthdasdcccc, getSelectedId, (authdasdcccc, id) => {
    const result = authdasdcccc.find(it => it['id'] === id);
    return result ? Object.assign({}, result) : undefined;
});

export const authdasdccccQuery = {
  getLoaded,
  getError,
  getAllAuthdasdcccc,
  getSelectedAuthdasdcccc
};
