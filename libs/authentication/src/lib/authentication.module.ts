import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { initialState as authInitialState, authReducer } from './+state/auth.reducer';
import { AuthEffects } from './+state/auth.effects';
import { AuthFacade } from './+state/auth.facade';
import { initialState as authdasdInitialState, authdasdReducer } from './+state/authdasd.reducer';
import { AuthdasdEffects } from './+state/authdasd.effects';
import { AuthdasdFacade } from './+state/authdasd.facade';
import { initialState as authdasdccccInitialState, authdasdccccReducer } from './+state/bob/authdasdcccc.reducer';
import { AuthdasdccccEffects } from './+state/bob/authdasdcccc.effects';
import { AuthdasdccccFacade } from './+state/bob/authdasdcccc.facade';
@NgModule({
  imports: [CommonModule, StoreModule.forFeature('auth', authReducer, { initialState: authInitialState }), EffectsModule.forFeature([AuthEffects]), StoreModule.forFeature('authdasd', authdasdReducer, { initialState: authdasdInitialState }), EffectsModule.forFeature([AuthdasdEffects]), StoreModule.forFeature('authdasdcccc', authdasdccccReducer, { initialState: authdasdccccInitialState }), EffectsModule.forFeature([AuthdasdccccEffects])],
  providers: [AuthFacade, AuthdasdFacade, AuthdasdccccFacade]
})
export class AuthenticationModule {}
